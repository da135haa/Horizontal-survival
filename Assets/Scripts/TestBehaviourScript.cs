﻿using UnityEngine;
using System.Collections;

public class TestBehaviourScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //1.物件名稱 2.建立時的座標(x,y,z) 3.建立時的角度(Quaternion.identity代表同一性)
        Instantiate(Resources.Load("Cube Test"), new Vector3(3, 1, (float)-4.5), Quaternion.identity);

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
