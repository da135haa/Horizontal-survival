﻿using UnityEngine;
using System.Collections;

public class NewBehaviourScript : MonoBehaviour
{
    //型別是我們所要使用的Component
    public TextMesh testText;
    public float i_update_time = 10;
    private float i_tmpUpdate_time = 0;

    void Awake()
    {
        Debug.Log("Awake");
        //該Object的名子,然後指定其中的Component
        testText = GameObject.Find("Test Text").GetComponent<TextMesh>();
    }
    // Use this for initialization
    void Start()
    {
        Debug.Log("Start");
        testText.text = "";
        i_tmpUpdate_time = 0;
    }

    // Update is called once per frame
    void Update()
    { 
        i_tmpUpdate_time += Time.deltaTime;
        //  每進來一次update就會＋一次 Time.deltaTime 每次今來的數字大概等於0.33333333
        if (i_tmpUpdate_time > i_update_time)
        {
            // 假設時間超過１０秒就來這裏
            testText.text = "Welcome unity3D!!!!!!";
            //  顯示這串字
            Destroy(gameObject.GetComponent<NewBehaviourScript>());
            //   移除自己這個腳本
        }
        else
        {
            //   沒超過十秒就來這裡
            int i_tmp = (int)i_tmpUpdate_time;
            testText.text = i_tmp.ToString();
            //  直接把秒數印出來
        }
    }
}
