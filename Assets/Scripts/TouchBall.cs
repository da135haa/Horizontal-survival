﻿using UnityEngine;
using System.Collections;

public class TouchBall : MonoBehaviour
{
    //OnMouseUpAsButton只有当鼠标在同一个GUIElement或Collider按下，在释放时调用。
    public int ballScore = 1;
    void OnMouseUpAsButton()
    {
        Camera.main.gameObject.SendMessage("scoreUpdate", ballScore);
        Destroy(gameObject);
    }
}
