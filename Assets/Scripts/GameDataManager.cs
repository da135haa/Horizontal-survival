﻿using UnityEngine;
using System.Collections;

public class GameDataManager : MonoBehaviour {

    // Use this for initialization
    public GameUIManager gameUIManager;
    public BallData ballData;
    // 因為要更新UI資料所以要FIND  01.UI上面的GameUIManager.cs
    void Start()
    {
        gameUIManager = GameObject.Find("01.GUI").GetComponent<GameUIManager>();
        init();

        //StartCoroutine(LoadAssetBunlder());
    }
    //把BallData實例化並使用也只有在init才會new一次  確保遊戲過程只有一個
    void init()
    {
        ballData = new BallData();
        ballData.score = 0;
        ballData.tatleScore = 0;
        ballData.count = 0;
    }
    //球每射出一顆就會呼叫這一次
    void shootUpdate()
    {
        ballData.count += 1;
        gameUIManager.shootUpdateForUI(ballData);
    }
    //球每被點擊中一顆就會呼叫這一次
    void scoreUpdate(int score)
    {
        ballData.tatleScore += score;
        gameUIManager.touchBallUpdateForUI(ballData);
    }
}
