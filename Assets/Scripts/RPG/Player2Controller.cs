﻿using UnityEngine;
using System.Collections;

public class Player2Controller : MonoBehaviour {
    //將圖檔資源放入,將要被控制的
    public Sprite[] playerSprite;
    //將要被指定的節點套入(導入此Script的物件)
    public SpriteRenderer spriteRenderer;
    //將要被指定的節點套入(導入此Script的物件)
    public Animator playerAnimator;

    public string[] controller;

    void Start()
    {
    }
    void Update()
    {
        //依序是下左右上
        if (Input.GetKey(KeyCode.S))
        {
            //更換此圖片
            spriteRenderer.sprite = playerSprite[0];
            //開始此動畫(沒設定停止不會自己停止 函式名稱是放在Animator中的Animation的名子)
            playerAnimator.Play(controller[0]);
            transform.position = new Vector3(transform.position.x, transform.position.y - 0.05f, transform.position.z);
        }
        if (Input.GetKey(KeyCode.A))
        {
            spriteRenderer.sprite = playerSprite[1];
            playerAnimator.Play(controller[1]);
            transform.position = new Vector3(transform.position.x - 0.05f, transform.position.y, transform.position.z);
        }
        if (Input.GetKey(KeyCode.D))
        {
            spriteRenderer.sprite = playerSprite[2];
            playerAnimator.Play(controller[2]);
            transform.position = new Vector3(transform.position.x + 0.05f, transform.position.y, transform.position.z);
        }
        if (Input.GetKey(KeyCode.W))
        {
            spriteRenderer.sprite = playerSprite[3];
            playerAnimator.Play(controller[3]);
            //套用此script的component的座標指定
            transform.position = new Vector3(transform.position.x, transform.position.y + 0.15f, transform.position.z);
        }

      
    }
}
