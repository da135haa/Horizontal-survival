﻿using UnityEngine;
using System.Collections;


public class ShootScript : MonoBehaviour {

    double time = 0;
    public double timeUp = 0.5;
    double destroyTime = 0;
    double destroyTimeUp = 2;
    public GameObject i;

    public AudioSource ShootAudio;

    //介由一開始的建立位置,來判斷該飛哪一個方向

    public float moveFlag;

    // Use this for initialization
    void Start()
    {
        moveFlag = transform.position.x;
    }


    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (time > timeUp)
        {   
            if(moveFlag ==-32)
            {
                //當時間經過1/3後就加速衝刺
                if (destroyTime > (destroyTimeUp / 3))
                {
                    transform.position = new Vector3(transform.position.x + 2f, transform.position.y, transform.position.z);
                }
                else
                {
                    transform.position = new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z);
                }
            }  
            else if(moveFlag ==32)
            {
                //當時間經過1/3後就加速衝刺
                if (destroyTime > (destroyTimeUp / 3))
                {
                    transform.position = new Vector3(transform.position.x - 2f, transform.position.y, transform.position.z);
                }
                else
                {
                    transform.position = new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z);
                }
            }
           



            time = 0;
        }

        destroyTime += Time.deltaTime;
        if (destroyTime > destroyTimeUp)
        {
            //Destroy(i);
            i.SetActive(false);
            destroyTime = 0;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.name == "Hero2")
        {
            ShootAudio.Play();
        }
        //要是對撞,兩個都銷毀
        else if(collision.gameObject.name == "HeroPrefab(Clone)")
        {
              Destroy(collision.gameObject);
            // Destroy(i.gameObject);

           // collision.gameObject.SetActive(false);
            i.SetActive(false);
        }
       
    }
}
