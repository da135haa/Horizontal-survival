﻿using UnityEngine;
using System.Collections;

public class MobileBg : MonoBehaviour {

    public GameObject bg;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        bg.transform.position = new Vector3(transform.position.x-0.4f, transform.position.y, transform.position.z);

        //當背景要是超過就移到一開始重新跑
        if(bg.transform.position.x < -76)
        {
            bg.transform.position = new Vector3(76, transform.position.y, transform.position.z);
        }
    }
}
