﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class BackgroundScript : MonoBehaviour {

    double floorTime = 0;
    public double instantiateFloorTime = 1;
    public Text textTime;
    System.Random rnd;

    public double instantiateShootTime = 1;
    double ShootTime = 0;

    double timeTag = 0;

    public GameObject[] bg;

    public GameObject hero;

    bool timeFlag = false;


    public float baseWidth = 1024;
    public float baseHeight = 768;
    public float baseOrthographicSize = 5;


    private Dictionary<int, GameObject> floorTotal;
    private int floorFlag = 0;

    private Dictionary<int, GameObject> shootTotal;
    private int shootFlag = 0;


    // Use this for initialization
    void Start () {
        rnd = new System.Random();
        //關於解析度的
        float newOrthographicSize = (float)Screen.height / (float)Screen.width * this.baseWidth / this.baseHeight * this.baseOrthographicSize;
        GetComponent<Camera>().orthographicSize = Mathf.Max(newOrthographicSize, this.baseOrthographicSize);

        
        floorTotal = new Dictionary<int, GameObject>();
        //給予初始物件
        GameObject valueFloor = (GameObject)Instantiate(Resources.Load("FloorPrefab"), new Vector2(35, -6 + (float)(rnd.Next(-1, 2) + rnd.NextDouble())), Quaternion.identity);
        //加入新的物件
        floorTotal.Add(floorFlag, valueFloor);
        floorFlag++;

        shootTotal = new Dictionary<int, GameObject>();
        //給予初始物件
        GameObject valueShoot = (GameObject)Instantiate(Resources.Load("ShootPrefab"), new Vector2(32, -6 + (float)(rnd.Next(1, 8) + rnd.NextDouble())), Quaternion.Euler(0, 0, 180));
        //加入新的物件
        shootTotal.Add(shootFlag, valueShoot);
        shootFlag++;

    }
	
	// Update is called once per frame
	void Update ()
    {
        //計時表
        if (timeFlag)
        {
            timeTag += Time.deltaTime;
            textTime.text = timeTag.ToString("#0.0000") + "sec";
        }
        //時間一到就生成一個地板物件
        floorTime += Time.deltaTime;

        if (floorTime > instantiateFloorTime)
        {   

            //先檢查當下有沒有關閉的,有的話就打開
            for(int i=0;i< floorFlag; i++)
            {   
                //要是有物件是關閉的,就打開並且給予新座標
                if(!floorTotal[i].activeInHierarchy)
                {
                    floorTotal[i].SetActive(true);
                    floorTotal[i].transform.position = new Vector2(35, -6 + (float)(rnd.Next(-1, 2) + rnd.NextDouble()));
                    break;
                }
                else if(i == floorFlag - 1)//要是到最後一個時,就創建新的物件
                {
                    GameObject valueFloor=(GameObject)Instantiate(Resources.Load("FloorPrefab"), new Vector2(35, -6 + (float)(rnd.Next(-1, 2) + rnd.NextDouble())), Quaternion.identity);
                    //加入新的物件
                    floorTotal.Add(floorFlag, valueFloor);
                    floorFlag++;
                    break;
                }
            }                 
            floorTime = 0;
        }
        //時間一到就生成一個攻擊物件
        ShootTime += Time.deltaTime;
        if (ShootTime > instantiateShootTime)
        {
            int r = Random.Range(0, 2);

            //判斷2個方向  左右         
            //控制生成的位置
            //先檢查當下有沒有關閉的,有的話就打開
            for (int i = 0; i < shootFlag; i++)
            {
                //要是有物件是關閉的,就打開並且給予新座標
                if (!shootTotal[i].activeInHierarchy)
                {
                    shootTotal[i].SetActive(true);                  
                    if (r == 0)
                    {
                        shootTotal[i].transform.position = new Vector2(-32, -6 + (float)(rnd.Next(1, 8) + rnd.NextDouble()));
                        shootTotal[i].transform.rotation = Quaternion.Euler(0, 0, 0);
                        shootTotal[i].GetComponent<ShootScript>().moveFlag = -32;
                    }
                    else if (r == 1)
                    {                      
                        shootTotal[i].transform.position = new Vector2(32, -6 + (float)(rnd.Next(1, 8) + rnd.NextDouble()));
                        shootTotal[i].transform.rotation = Quaternion.Euler(0, 0, 180);
                        shootTotal[i].GetComponent<ShootScript>().moveFlag = 32;
                    }

                    break;
                }
                else if (i == shootFlag - 1)//要是到最後一個時,就創建新的物件
                {
                    GameObject valueShoot=null;
                    if (r == 0)
                    {
                        valueShoot=(GameObject)Instantiate(Resources.Load("ShootPrefab"), new Vector2(-32, -6 + (float)(rnd.Next(1, 8) + rnd.NextDouble())), Quaternion.Euler(0, 0, 0));
                    }
                    else if (r == 1)
                    {
                        valueShoot = (GameObject)Instantiate(Resources.Load("ShootPrefab"), new Vector2(32, -6 + (float)(rnd.Next(1, 8) + rnd.NextDouble())), Quaternion.Euler(0, 0, 180));
                    }
                    //加入新的物件
                    shootTotal.Add(shootFlag, valueShoot);
                    shootFlag++;
                    break;
                }
            }



            ShootTime = 0;
        }

        if(!hero.activeSelf)
        { 
            timeFlag = false;
            timeTag = 0;
        }
        else
        {
            timeFlag = true;
            
        }

    }


}
