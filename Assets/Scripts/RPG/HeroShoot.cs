﻿using UnityEngine;
using System.Collections;


public class HeroShoot : MonoBehaviour {


    public double timeUp = 0.5;
    double destroyTime = 0;
    double destroyTimeUp = 0.4;
    public GameObject i;



    private DIR dir;

    public void setDir(DIR dir)
    {
        this.dir = dir;
    }

    // Use this for initialization
    void Start()
    {
        //Debug.Log(transform.rotation.z);
    }
    // Update is called once per frame
    void Update()
    {

        //用指向角度來判斷飛的方向 下左右上
        if (dir == DIR.DOWN)
        {     
            transform.position = new Vector3(transform.position.x, transform.position.y - 1f, transform.position.z);
        }
        else if (dir == DIR.LEFT)
        {
            transform.position = new Vector3(transform.position.x - 1f, transform.position.y, transform.position.z);
        }
        else if (dir == DIR.RIGHT)
        {
            transform.position = new Vector3(transform.position.x + 1f, transform.position.y, transform.position.z);
        }
        else if (dir == DIR.UP)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z);
        }

        destroyTime += Time.deltaTime;

        if (destroyTime > destroyTimeUp)
        {
            Destroy(i);
            destroyTime = 0;
        }

    }
}