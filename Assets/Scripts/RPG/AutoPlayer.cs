﻿using UnityEngine;
using System.Collections;
using System;


public class AutoPlayer : MonoBehaviour {

    //將圖檔資源放入,將要被控制的
   // public Sprite[] playerSprite;
    //將要被指定的節點套入(導入此Script的物件)
  //  public SpriteRenderer spriteRenderer;
    //將要被指定的節點套入(導入此Script的物件)

    public Animator playerAnimator;
    public string[] controller;
    float time = 0;
    public float timeUp;
    System.Random rnd;

    void Start()
    {
        rnd = new System.Random();
    }
    void Update()
    {
        time += Time.deltaTime;
        //依序是下左右上
        if (time > timeUp)
        {
            int value = rnd.Next(0,4);
            if(value == 0)
            {
                playerAnimator.Play(controller[0]);
                transform.position = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);
            }
            else if(value == 1)
            {
                playerAnimator.Play(controller[1]);
                transform.position = new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z);
            }
            else if (value == 2)
            {
                playerAnimator.Play(controller[2]);
                transform.position = new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z);
            }
            else if (value == 3)
            {
                playerAnimator.Play(controller[3]);
                transform.position = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);
            }

            time = 0;
        }   
    }
}
