﻿using UnityEngine;
using System.Collections;



public class MobileFloor : MonoBehaviour {

    double time = 0;
    public double timeUp = 0.5;
    double destroyTime = 0;
    double destroyTimeUp = 5;
    public GameObject i;


    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {

        time += Time.deltaTime;
        if (time > timeUp)
        {
            transform.position = new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z);
            time = 0;
        }
       
        destroyTime += Time.deltaTime;
        if (destroyTime > destroyTimeUp)
        {
            //Destroy(i);
            //時間到就隱藏起來
            i.SetActive(false);

            destroyTime = 0;
        }
    
    }
}
