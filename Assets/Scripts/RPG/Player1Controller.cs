﻿using UnityEngine;

using System.Collections;

using UnityEngine.UI;

public class Player1Controller : MonoBehaviour
{

    //將要被指定的節點套入(導入此Script的物件)
    public SpriteRenderer spriteRenderer;
    //將要被指定的節點套入(導入此Script的物件)
    public Animator playerAnimator;

    public string[] controller;

    //跳躍音效
    public AudioClip[] jumpAudio;

    int jump = 0;
    //射擊cd時間
    public double shootCD = 0.5;
    double shootTime = 0;

    public Text lifeText;



    public GameObject i;

    public GameObject gameOver;
    public GameObject inputName;
  
    public Button buttonUp;
    public Button buttonDown;
    public Button buttonLeft;
    public Button buttonRight;
    public Button buttonShoot;

    private int leftOrRight = 1;



    private JoyStick js;

    void Start()
    {
        js = GameObject.FindObjectOfType<JoyStick>();
        js.OnJoyStickTouchMove += OnJoyStickMove;
        js.OnJoyStickTouchEnd += OnJoyStickEnd;
    }
    //移動時
    void OnJoyStickMove(Vector2 vec)
    {
        //设置角色朝向
       // Quaternion q = Quaternion.LookRotation(new Vector3(vec.x, 0, vec.y));
      //  transform.rotation = q;
        //移动角色并播放奔跑动画
        //transform.Translate(Vector3.forward * 75f * Time.deltaTime);

        //左右上下
      
        if (vec.x < 0)
        {
            leftOrRight = 1;
            spriteRenderer.flipX = true;
            playerAnimator.Play(controller[1]);
            transform.position = new Vector3(transform.position.x - 0.05f, transform.position.y, transform.position.z);
        }
        if (vec.x > 0)
        {
            leftOrRight = 0;
            spriteRenderer.flipX = false;
            playerAnimator.Play(controller[1]);
            transform.position = new Vector3(transform.position.x + 0.05f, transform.position.y, transform.position.z);
        }
        if (vec.y > 0)
        {
            if (jump > 0)
            {
                if (jump == 10)
                {
                    //跳躍音效
                    int i = Random.Range(0, jumpAudio.Length);
                    //播放哪首、播放的位置在哪
                    AudioSource.PlayClipAtPoint(jumpAudio[i], transform.position, 100);

                }

                playerAnimator.Play(controller[2]);


                //套用此script的component的座標指定(跳躍高度)
                transform.position = new Vector3(transform.position.x, transform.position.y + 0.2f, transform.position.z);
                jump -= 1;
            }
        }
        if (vec.y < 0)
        {
            playerAnimator.Play(controller[0]);
        }
    }
    void OnJoyStickEnd()
    {
        playerAnimator.Play(controller[4]);

    }

    void Update()
    {
        shootTime += Time.deltaTime;

        //依序是下左右上
        if (Input.GetKey(KeyCode.DownArrow)|| buttonDown.GetComponent<VirtualKeyboard>().keyboardFlag)
        {

            //開始此動畫(沒設定停止不會自己停止 函式名稱是放在Animator中的Animation的名子)
            playerAnimator.Play(controller[0]);
            transform.position = new Vector3(transform.position.x, transform.position.y - 0.05f, transform.position.z);
        }
        if (Input.GetKey(KeyCode.LeftArrow) || buttonLeft.GetComponent<VirtualKeyboard>().keyboardFlag)
        {
            spriteRenderer.flipX = true;
            leftOrRight = 1;

            playerAnimator.Play(controller[1]);
            transform.position = new Vector3(transform.position.x - 0.05f, transform.position.y, transform.position.z);
        }
        if (Input.GetKey(KeyCode.RightArrow) || buttonRight.GetComponent<VirtualKeyboard>().keyboardFlag)
        {
            spriteRenderer.flipX = false;
            leftOrRight = 0;

            playerAnimator.Play(controller[1]);
            transform.position = new Vector3(transform.position.x + 0.05f, transform.position.y, transform.position.z);
        }
        if (Input.GetKey(KeyCode.UpArrow) || buttonUp.GetComponent<VirtualKeyboard>().keyboardFlag)
        {
            if (jump > 0)
            {
                if (jump == 10)
                {
                    //跳躍音效
                    int i = Random.Range(0, jumpAudio.Length);
                    //播放哪首、播放的位置在哪
                    AudioSource.PlayClipAtPoint(jumpAudio[i], transform.position,100);
                    
                }

                playerAnimator.Play(controller[2]);


                //套用此script的component的座標指定(跳躍高度)
                transform.position = new Vector3(transform.position.x, transform.position.y + 0.2f, transform.position.z);
                jump -= 1;
            }
        }


        if (Input.GetKey(KeyCode.Space) || buttonShoot.GetComponent<VirtualKeyboard>().keyboardFlag)
        {
            //左右射級
            if (shootTime>shootCD)
            {
                playerAnimator.Play(controller[3]);

                InvokeRepeating("shoot", 0.6f,0);
                            
                shootTime = 0;
            }
            
        }
        //掉落摔死或超出邊介的部分
        if (transform.position.y < -20 || transform.position.x < -20 || transform.position.x > 20)
        {
            //沒命了就自己銷毀
            if (System.Int32.Parse(lifeText.text)==0)
            {
                i.SetActive(false);
                gameOver.SetActive(true);
                inputName.SetActive(true);
            }
            else
            {
                transform.position = new Vector3(0, 10, transform.position.z);
                lifeText.text = (System.Int32.Parse(lifeText.text) - 1).ToString();
            }   
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        jump = 10;
    }

    void shoot()
    {
        if (leftOrRight == 1)
        {

            GameObject obj = (GameObject)Instantiate(Resources.Load("HeroPrefab"), new Vector3(transform.position.x - 2f, transform.position.y, transform.position.z), Quaternion.Euler(0, 0, 90));
            obj.GetComponent<HeroShoot>().setDir(DIR.LEFT);
        }
        else if (leftOrRight == 0)
        {
            GameObject obj = (GameObject)Instantiate(Resources.Load("HeroPrefab"), new Vector3(transform.position.x + 2f, transform.position.y, transform.position.z), Quaternion.Euler(0, 0, 270));
            obj.GetComponent<HeroShoot>().setDir(DIR.RIGHT);
        }
    }
}

