﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


//通用管理對象池的class
public class Pooling : MonoBehaviour {

    #region 公共属性
    /// <summary>
    /// 池中所使用的元素Prefab
    /// </summary>
    public GameObject ObjPrefab;

    /// <summary>
    /// 初始容量
    /// </summary>
    public int InitialCapacity;

    #endregion

    #region 私有属性

    /// <summary>
    /// 初始下标
    /// </summary>
    private int _startCapacityIndex;

    /// <summary>
    /// 可用下标
    /// </summary>
    private List<int> _avaliableIndex;

    /// <summary>
    /// 池中全部元素
    /// </summary>
    private Dictionary<int, GameObject> _totalObjList;

    #endregion

    #region 事件/重写方法

    void Start()
    {      
        _avaliableIndex = new List<int>(InitialCapacity);
        _totalObjList = new Dictionary<int, GameObject>(InitialCapacity);
        //啟用封裝好的自製方法
        expandPool();
    }
    #endregion

    #region 公共方法

    /// <summary>
    /// 取得一个物体，返回值 1,obj代表，ID是1的物体被取到，ID可以用来归还物体的时候用到
    /// </summary>
    /// <returns></returns>
    public KeyValuePair<int, GameObject> PickObj()
    {
        
        if (_avaliableIndex.Count == 0)
            expandPool();

        int id = _avaliableIndex[0];
        _avaliableIndex.Remove(id);

        _totalObjList[id].SetActive(true);
        return new KeyValuePair<int, GameObject>(id, _totalObjList[id]);
    }

    /// <summary>
    /// 从池中取出元素，在制定时间后回收
    /// </summary>
    /// <param name="existSecond"></param>
    /// <returns></returns>
    public KeyValuePair<int, GameObject> PickObjWithDelayRecyle(float existSecond)
    {
        KeyValuePair<int, GameObject> obj = PickObj();
        StartCoroutine(startRecycleExplosion(obj.Key, existSecond));
        return obj;
    }


    /// <summary>
    /// 回收一个物体
    /// </summary>
    /// <param name="id"></param>
    public void RecyleObj(int id)
    {
        //隱藏、回到最初的位置、回去對象池
        _totalObjList[id].SetActive(false);
        _totalObjList[id].transform.parent = transform;
        _avaliableIndex.Add(id);
    }

    #endregion

    #region 私有方法

    IEnumerator startRecycleExplosion(int id, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        RecyleObj(id);
    }

    /// <summary>
    /// 扩展池
    /// </summary>
    private void expandPool()
    {
        int start = _startCapacityIndex;
        int end = _startCapacityIndex + InitialCapacity;

        for (int i = start; i < end; i++)
        {
            //要是有重複的就continue
            if (_totalObjList.ContainsKey(i))
                continue;

            //具現化後的Object先用as檢察一下再賦值
            GameObject newObj = Instantiate(ObjPrefab) as GameObject;
            //先不顯示
            newObj.SetActive(false);
            //將該物件的編號加入List,而key和value加入Dictionary之中
            _avaliableIndex.Add(i);
            _totalObjList.Add(i, newObj);
        }
        _startCapacityIndex = end;     
    }
    #endregion
}