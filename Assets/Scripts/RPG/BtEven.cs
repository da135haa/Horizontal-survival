﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using Mono.Data.Sqlite;
using System;

public class BtEven : MonoBehaviour
{
    public Text editText;
    public Text timeText;

    Database db;//資料庫物件
    string databaseName = "Yang.db";//資料庫名稱
    string tableName = "Yu";//資料庫內的資料表名稱
    SqliteDataReader reader;//搜尋資料表的資料

    public GameObject inputName;
    public GameObject ranking;
    public GameObject gameOver;
    public GameObject hero;

    public Text lifeText;

    public Text rankingText;
    public Text rankingNameText;
    public Text rankingTimeText;

    void Start()
    {
        //建立資料庫，同時進行連線，也可改寫成下面被註解的那兩行，由於不同平台所使用的連線語法都不一樣，所以我已經先預設好了。若要修改或得知Windows路徑則到Database.cs內搜尋「Windows端的資料庫存放位置」；Android則因為諸多限制，所以不建議修改。
        db = new Database(databaseName);
        //開啟資料庫
        db.openDatabaseConnecting();

        //確認是否已有指定的資料表，若沒有則創造該資料表同時插入資料
        if (db.isTableExists(tableName) == false)
        {
            //前面是標題、後面對應的是型態,TEXT為SQLite的字串型態，INTEGER為SQLite的整數型態
            db.createTable(tableName, new string[] { "Name", "Time" }, new string[] { "TEXT", "TEXT" });
        }

        //顯示第一次記分畫面
        rankingUpdate();

    }
    //將資料存進去並且印出來
    public void inputNameOnClick()
    {
        //資料庫的字串資料必須使用單引號框起來'Yang'         
        string name = "'" + editText.text + "'";
        string time = "'" + timeText.text.Trim().TrimEnd("sec".ToCharArray()) + "'";
        //Debug.Log("名子:" + name);
        //Debug.Log("時間:" + time);
        //存入  
        db.insertInto(tableName, new string[] { name, time });

        //將輸入名稱關閉,打開計分表
        inputName.SetActive(false);
        gameOver.SetActive(false);
        ranking.SetActive(true);
        //刷新記分板
        rankingUpdate();



        //取出
        string test = "";
        reader = db.searchFullTable(tableName);
        string[] dataName = db.readStringData(reader, "Name");
        //每一次都要重新search
        reader = db.searchFullTable(tableName);
        string[] dataTime = db.readStringData(reader, "Time");

        //Debug.Log("陣列長度:" + dataTime.Length);
        //將全部內容抓出來
        for (int i = 0; i < dataTime.Length; i++)
        {
            test += dataName[i].ToString() + "  " + dataTime[i].ToString() + "\n";
        }

    }

    public void gameGO()
    {
        lifeText.text = "2";
      

        ranking.SetActive(false);

        hero.SetActive(true);
        hero.transform.position = new Vector3(0, 10, transform.position.z);
    }

    //刷新記分板的方法
    public void rankingUpdate()
    {
        //先將全部洗白
        rankingText.text = "";
        rankingNameText.text = "";
        rankingTimeText.text = "";

        //取出
        reader = db.searchFullTable(tableName);
        string[] dataName = db.readStringData(reader, "Name");
        //每一次都要重新search
        reader = db.searchFullTable(tableName);
        string[] dataTime = db.readStringData(reader, "Time");




        // 氣泡排序法，由小到大排序
        for (int i = 0; i < dataTime.Length; i++)
        {
            for (int j = i + 1; j < dataTime.Length; j++)
            {
                if (double.Parse(dataTime[i]) < double.Parse(dataTime[j]))
                {
                    string temp = dataTime[i];
                    dataTime[i] = dataTime[j];
                    dataTime[j] = temp;

                    temp = dataName[i];
                    dataName[i] = dataName[j];
                    dataName[j] = temp;
                }
            }
        }

        for(int i=0;i<dataTime.Length;i++)
        {
            rankingText.text += (i + 1) + ".\n";

            rankingNameText.text += dataName[i] + "\n";

            rankingTimeText.text += dataTime[i] + "sec\n";
        }
    }

    void OnDisable()
    {
        //當該程式碼所放置的物件被結束時關閉資料庫連線
        db.closeDatabaseConnecting();
    }
    void OnDestroy()
    {
        //釋放資料庫
        db.releaseDatabaseAllResources();
    }
}


