﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;// 使用事件系統

public class Click : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler
{

    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("Enter ###.");
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("Click ###.");
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("Exit ###.");
    }

}
