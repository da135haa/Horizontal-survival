﻿using UnityEngine;
using System.Collections;

public class GameUIManager : MonoBehaviour {

    public TextMesh ballCount;
    public TextMesh gameName;
    public TextMesh scoreCount;

    // Use this for initialization
    void Start () {
        ballCount = GameObject.Find("BallCount").GetComponent<TextMesh>();
        gameName = GameObject.Find("GameName").GetComponent<TextMesh>();
        scoreCount = GameObject.Find("ScoreCount").GetComponent<TextMesh>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    //自製發射方法
    public void shootUpdateForUI(BallData data)
    {
        ballCount.text = data.count.ToString();
    }

    public void touchBallUpdateForUI(BallData data)
    {
        scoreCount.text = data.tatleScore.ToString();
    }
}
