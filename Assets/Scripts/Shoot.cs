﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour
{
    // Use this for initialization
    public float f_shootTime = 1;//每一秒射一次
    float f_tmpShootTime = 0;//只是為了計算過了幾毫秒
    //設定球要射出的力道  的最大最小值
    public int i_shootPos_x = 350;
    public int i_shootPos_y = 450;
    public int i_shootPos_z = 100;

    void Start()
    {   //初始化
        init();
    }

    // Update is called once per frame
    void Update()
    {
        if (isShoot())//如果這裡回傳true就設球  寫在下面
        {
            shoot("Ball"); //發射球傳入字串是因為要去Resource取得這球
        }

    }
    void init()
    {
        f_tmpShootTime = 0;
    }
    //判斷是否可以發射了
    bool isShoot()
    {
        bool isshoot;
        f_tmpShootTime += Time.deltaTime;
        //這邊只用到一個API  Time.deltaTime 這是每一禎就會回傳上一禎到這一禎經過了幾毫秒的API 設下只是計算是否大於以秒要做甚麼
        if (f_tmpShootTime > f_shootTime)
        {
            isshoot = true;
            f_tmpShootTime = Time.deltaTime;
        }
        else
        {
            isshoot = false;
        }
        return isshoot;
    }
    //射球就靠這裡了
    void shoot(string name)
    {
        //Instantiate主要是實例化物件
        //Resources.Load主要是取得Resources裡面的prefab來使用所以我有在OBS這資料夾下方了一個Ball的Prefab
        GameObject ball = Instantiate(Resources.Load("OBS/" + name), gameObject.transform.position, gameObject.transform.rotation) as GameObject;
        //AddComponent是替這物件加上這腳本
        ball.AddComponent<TouchBall>();//這個腳本在下面
        ball.AddComponent<Rigidbody>();//幫他增加鋼体這樣才能使用物理引擎
        ball.GetComponent<TouchBall>().ballScore = 2;//這個腳本上有個參數可以設定
        ball.GetComponent<Rigidbody>().useGravity = true;//使用物理引擎
        ball.GetComponent<SphereCollider>().isTrigger = true;//是否可以穿透

        Camera.main.gameObject.SendMessage("shootUpdate");//這裡就是呼叫剛剛在camera上面的腳本寫的方法用的  很方便吧
        ball.transform.GetComponent<Rigidbody>().AddForce(setShootForce());//這就是位甚麼要加鋼体的原因才能使用AddForce給一個力道讓球飛出去

        Destroy(ball, 3);//3秒後移除這個ball
    }
    //這邊是設定要發射的力道是多少
    Vector3 setShootForce()
    {
        int tmpX = Random.Range(-i_shootPos_x, i_shootPos_x);
        Vector3 tmpVector3 = new Vector3(tmpX, i_shootPos_y, i_shootPos_z);
        return tmpVector3;
    }

}